package com.example.davaleban4

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.navigation.Navigation.findNavController
import androidx.navigation.findNavController
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.material.bottomnavigation.BottomNavigationItemView
import com.google.android.material.bottomnavigation.BottomNavigationView

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navigationViewer : BottomNavigationView = findViewById(R.id.nav_id)

        val navigationController = findNavController(R.id.fragment)

        val appBarConfig = AppBarConfiguration(setOf(
            R.id.navigation_home , R.id.navigation_dashboard , R.id.navigation_nofitication , R.id.about
        ))

        setupActionBarWithNavController(navigationController,appBarConfig)
        navigationViewer.setupWithNavController(navigationController)
    }

}





